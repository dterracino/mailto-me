# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## [Unreleased]

- Nothing at the moment

## [0.1.0] - 2019-09-27

Initial release

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Unreleased]: https://gitlab.com/cherrypicker/mailto-me/compare/0.1.0...master
[0.1.0]: https://gitlab.com/cherrypicker/mailto-me/-/tags/0.1.0
